
1. The uPiHat PCB is to be powered by a 12V source voltage, with a maximum current of 13A.
2. The PCB is to be connected to a Raspberry Pi Zero for full functionality.
3. The Pi zero GPIO pins 17, 22 and 27 are configured to convert the input from the motor feedback to logic that will power either of the LED’s connected to those pins
4. GPIO pin 4 is configured to output a suitable PWM signal
