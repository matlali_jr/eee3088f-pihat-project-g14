# EEE3088F-piHat-project-G6

Group project repository for uploading information concerning the microPiHat that will be designed for a Raspberry Pi Zero

Git repository for EEE3088F group 6's PiHat Project
The microHat acts as an interface between a Raspberry Pi Zero and a servo motor. This makes it possible to drive a motor with the Raspberry Pi which would otherwise not be ideal as the Raspberry Pi cannot source the required voltage to operate most motors.

This repository contains the design and instructions of manufcture and use of this specific piHat
