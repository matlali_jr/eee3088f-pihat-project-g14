EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C1
U 1 1 60B9CEEE
P 6200 4100
F 0 "C1" H 6315 4146 50  0000 L CNN
F 1 "C" H 6315 4055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 6238 3950 50  0001 C CNN
F 3 "~" H 6200 4100 50  0001 C CNN
	1    6200 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 60B9DA75
P 5200 4100
F 0 "D2" H 5200 4316 50  0000 C CNN
F 1 "D_Zener" H 5200 4225 50  0000 C CNN
F 2 "Diode_THT:Diode_Bridge_15.1x15.1x6.3mm_P10.9mm" H 5200 4100 50  0001 C CNN
F 3 "~" H 5200 4100 50  0001 C CNN
	1    5200 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 2950 5200 2950
Wire Wire Line
	5200 3950 5200 2950
Connection ~ 5200 2950
Wire Wire Line
	5200 2950 5550 2950
Wire Wire Line
	6200 2950 5850 2950
Wire Wire Line
	6200 2950 6200 3950
Wire Wire Line
	6200 4200 6200 4250
Wire Wire Line
	6200 5050 5200 5050
Connection ~ 6200 4250
Wire Wire Line
	6200 4250 6200 5050
Wire Wire Line
	5200 4250 5200 5050
Wire Wire Line
	4400 3250 4400 3700
Wire Wire Line
	4200 2950 3700 2950
Wire Wire Line
	3700 2950 3700 3650
Connection ~ 5200 5050
Wire Wire Line
	5200 5050 3700 5050
Wire Wire Line
	3700 3900 3700 5050
Text GLabel 4400 3700 0    50   Input ~ 0
GPIO4
Text GLabel 3700 3650 0    50   Input ~ 0
Vin+
Text GLabel 3700 3900 0    50   Input ~ 0
GND
Wire Wire Line
	6200 2950 7200 2950
Wire Wire Line
	7200 2950 7200 3950
Connection ~ 6200 2950
Wire Wire Line
	6200 5050 7200 5050
Wire Wire Line
	7200 5050 7200 4250
Connection ~ 6200 5050
Text GLabel 7200 4250 0    50   Input ~ 0
GND
Text GLabel 7200 3950 0    50   Input ~ 0
Vout+
$Comp
L Device:Q_NMOS_DGS Q1
U 1 1 60B9FB78
P 4400 3050
F 0 "Q1" V 4743 3050 50  0000 C CNN
F 1 "Q_NMOS_DGS" V 4652 3050 50  0000 C CNN
F 2 "Package_DirectFET:DirectFET_MN" H 4600 3150 50  0001 C CNN
F 3 "~" H 4400 3050 50  0001 C CNN
	1    4400 3050
	0    -1   -1   0   
$EndComp
$Comp
L Device:L_Core_Ferrite L1
U 1 1 60BA2913
P 5700 2950
F 0 "L1" V 5925 2950 50  0000 C CNN
F 1 "L_Core_Ferrite" V 5834 2950 50  0000 C CNN
F 2 "Connector_JAE:JAE_LY20-4P-DLT1_2x02_P2.00mm_Horizontal" H 5700 2950 50  0001 C CNN
F 3 "~" H 5700 2950 50  0001 C CNN
	1    5700 2950
	0    -1   -1   0   
$EndComp
Text Notes 7950 7350 0    50   ~ 0
MVMAMA001. STLTSE004, MLXBAT001
Text Notes 7950 7350 0    50   ~ 0
AMPLIFIER CIRCUIT\n
Text Notes 8650 7500 0    50   ~ 0
01/06/2021\n
$EndSCHEMATC
